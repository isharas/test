<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageUpload extends Model
{
    protected $fillable = [ 'image_name','employee_id' ];

    public function emplo() {
        return $this->belongsTo('Employee::class'); }

}
