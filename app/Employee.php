<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'name', 'address'
    ];

    public function employeecity()
    {
        return $this->hasOne(City::class, 'employee_id');
    }
}
