<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class MapController extends Controller
{
    public function locationAutoComplete(Request $request) {

        return view('auto-complete.autocomplete');

    }
    public function index() {
        $address = Employee::all();
        return view('auto-complete.index');

    }
}
